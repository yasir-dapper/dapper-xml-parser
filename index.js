const DOMParser = require('xmldom').DOMParser;
const {xmlToJson} = require('./lib/xmltojson')
const nodeFetch = require('node-fetch')

const parseFromURL = url => {
	return new Promise((resolve, reject) => {
		let validationMessage = validateURL(url)
		if(validationMessage) {
			reject({ message: validationMessage})
		}
		if(!fetch) {
			fetch = nodeFetch
		}
		fetch(url)
		.then(res => res.text())
		.then(res => {
			try {
				var doc = new DOMParser().parseFromString(res, 'text/xml');
				let parsedJSON = xmlToJson(doc)
				resolve(parsedJSON)
			} catch (e) {
				reject(e)
			}
		}).catch(err => {
			reject(err)
		})	
	})
}

let validateURL = url => {
	let message = ''
	if( typeof url !== "string" ) {
		message = "URL must be a string!"
	} else if (!url) {
		message = "Please provide a valid URL"
	}
	return message
}

module.exports = {
	parseFromURL,
	default: { parseFromURL }
}
