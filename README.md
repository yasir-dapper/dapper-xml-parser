#XML Parser

## How to use:
- Add package dependency to your `package.json`. Under dependencies, add `"dapper-kml-parser": "https://bitbucket.org/yasir-dapper/dapper-xml-parser",`.
- Import and use the package
```
...
//ES5
const { parseFromURL } = require('dapper-xml-parser')
//ES6
import { parseFromURL } from 'dapper-xml-parser'

//Your code
...
...
parseFromURL(someURL)
.then(response => doSomething(response))
.catch(err => doSomethingElse(err))


```